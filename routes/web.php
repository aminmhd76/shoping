<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/






Route::group(['namespace' => 'Frontend' ] , function (){

    Route::get('login' , 'UserController@login')->name('login');
    Route::post('login' , 'UserController@doLogin')->name('post.login');
    Route::get('register' , 'UserController@register')->name('register');
    Route::post('register' , 'UserController@doRegister')->name('post.register');
    Route::get('logout' , 'UserController@logout')->name('logout');


//files
    Route::get('/' , 'HomeController@index')->name('frontend.home');
    Route::get('file/details/{file_id}' , 'FilesController@details')->name('frontend.file.details');
    // Route download ........
//plans
    Route::get('plans' , 'PlansController@index')->name('frontend.plan.index');
    Route::get('subscribe/{plan_id}' , 'SubscribesController@register')->name('frontend.subscribe.register');
    Route::post('subscribe/{plan_id}' , 'SubscribesController@index')->name('frontend.subscribe.index');
    Route::get('download/{file_id}' , 'FilesController@download')->name('frontend.file.download');








});





// Admin
Route::group(['prefix' => 'admin' , 'namespace' => 'Admin' , 'middleware' => 'check'] , function () {
    Route::get('user', 'UserController@index')->name('admin.user.first');
    Route::get('list', 'UserController@list')->name('admin.user.list');
    Route::get('create', 'UserController@create')->name('admin.user.create');
    Route::post('create', 'UserController@store')->name('admin.user.store');
    Route::get('delete/{user_id}', 'UserController@delete')->name('admin.user.delete');
    Route::get('edit/{user_id}', 'UserController@edit')->name('admin.user.edit');
    Route::post('edit/{user_id}', 'UserController@update')->name('admin.user.update');
    Route::get('user/package/{user_id}' , 'UserController@package')->name('admin.user.package');
//files
    Route::get('file', 'FilesController@index')->name('admin.file.index');
    Route::get('file/create', 'FilesController@create')->name('admin.file.create');
    Route::post('file/create', 'FilesController@store')->name('admin.file.store');
    Route::get('file/destroy/{file_id}', 'FilesController@delete')->name('admin.file.destroy');
//plans
    Route::get('plan/create', 'PlansController@create')->name('admin.plan.create');
    Route::post('plan/create', 'PlansController@store')->name('admin.plan.store');
    Route::get('plan/list', 'PlansController@list')->name('admin.plan.list');
    Route::get('plan/delete/{plan_id}', 'PlansController@delete')->name('admin.plan.delete');
    Route::get('plan/edit/{plan_id}', 'PlansController@edit')->name('admin.plan.edit');
    Route::post('plan/edit/{plan_id}', 'PlansController@update')->name('admin.plan.update');
//packages
    Route::get('package/create', 'PackagesController@create')->name('admin.package.create');
    Route::post('package/create', 'PackagesController@store')->name('admin.package.store');
    Route::get('package/list', 'PackagesController@list')->name('admin.package.list');
    Route::get('package/delete/{package_id}', 'PackagesController@delete')->name('admin.package.delete');
    Route::get('package/edit/{package_id}', 'PackagesController@edit')->name('admin.package.edit');
    Route::post('package/edit/{package_id}', 'PackagesController@update')->name('admin.package.update');
    Route::get('package/file/{package_id}', 'PackagesController@sync')->name('admin.package.sync');
    Route::post('package/file/{package_id}', 'PackagesController@choose')->name('admin.package.choose');
//payments
    Route::get('payment/list' , 'PaymentsController@list')->name('admin.payment.list');
//category
    Route::get('category/create' , 'CategoryController@create')->name('admin.category.create');
    Route::post('category/create' , 'CategoryController@store')->name('admin.category.store');
    Route::get('category/list' , 'CategoryController@list')->name('admin.category.list');
    Route::get('category/delete/{category_id}' , 'CategoryController@delete')->name('admin.category.delete');
    Route::get('category/edit/{category_id}' , 'CategoryController@edit')->name('admin.category.edit');
    Route::post('category/edit/{category_id}' , 'CategoryController@update')->name('admin.category.update');



});
