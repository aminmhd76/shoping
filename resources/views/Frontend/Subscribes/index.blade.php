@extends('layouts.frontend')



@section('content')
    @if($subscribe && !empty($subscribe))
        <table class="table table-bordered table-condensed table-hover">
            <tr>
                <td>عنوان</td>
                <td>{{ $subscribe->plan_tittle }}</td>
            </tr>
            <tr>
                <td>قیمت</td>
                <td>{{ number_format($subscribe->plan_price) }}</td>
            </tr>
            <tr>
                <td>محدودیت دانلو روزانه</td>
                <td>{{ $subscribe->plan_limit_download_count }}</td>
            </tr>
            <tr>
                <td>تعداد روز</td>
                <td>{{  $subscribe->plan_days_count }}</td>
            </tr>
        </table>
        <form action="{{ Route('frontend.subscribe.index' , $subscribe->plan_id ) }}" method="post">
            @csrf
            <div class="form-group">
                <input type="hidden" name="plan_id" value="{{ $subscribe->plan_id }}">
                <button class="btn btn-success">خرید این طرح</button>
            </div>
        </form>
    @endif

@endsection
