@extends('layouts.frontend')


@section('content')

    <div class="row">
        <div class="col-xs-12 col-md-6">
            @include('partials.error')
            <form action="" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">نام کامل :</label>
                    <input class="form-control" id="name" name="name" type="text" >
                </div>
                <div class="form-group">
                    <label for="email">ایمیل :</label>
                    <input class="form-control" id="email" name="email" type="email" >
                </div>
                <div class="form-group">
                    <label for="password">کلمه عبور :</label>
                    <input class="form-control" id="password" name="password" type="password">
                </div>
                <div class="form-group">
                    <input type="submit" name="submit" class="btn btn-success" value="ثبت نام">
                </div>
            </form>
        </div>
    </div>


@endsection
