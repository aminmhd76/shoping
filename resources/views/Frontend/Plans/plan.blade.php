@extends('layouts.frontend')


@section('content')
    <table class="table table-bordered table-condensed table-hover">
        <thead>
        <tr>
            <th>عنوان</th>
            <th>قیمت</th>
            <th>تعداد روز</th>
            <th>محدودیت دانلود روزانه</th>
            <th>عملیات</th>
        </tr>

        </thead>
        <tbody>
        @if($plan_Item && count($plan_Item))
            @foreach($plan_Item as $plans)
                <tr>
                    <td>{{ $plans->plan_tittle }}</td>
                    <td>{{ $plans->plan_price }}</td>
                    <td>{{ $plans->plan_days_count }}</td>
                    <td>{{ $plans->plan_limit_download_count }}</td>
                    <td>
                        <a href="{{ Route('frontend.subscribe.register' , [$plans->plan_id]) }}"
                           class="btn btn-success visible-lg-block">خرید</a>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>


    </table>


@endsection
