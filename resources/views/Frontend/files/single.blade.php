@extends('layouts.frontend')



@section('content')


    @if( !empty($files) )
        <style>
            .bold-span {
                font-weight: bold;
            }
        </style>
        <p>
           <span class="bold-span">عنوان فایل:</span>
            <span>{{ $files->file_tittle }}</span>
        </p>
        <p>
            <span class="bold-span">توضیحات فایل:</span>
            <span>{{ $files->file_description }}</span>
        </p>
        <p>
            <span class="bold-span">نوع فایل:</span>
            <span>{{ $files->file_type }}</span>
        </p>
            <span class="bold-span">تاریخ ثبت:</span>
            <span> Date-> {{   date("d-m-Y", strtotime($files->created_at)) }}</span>

        @if(App\Utility\Utility::is_user_subscribed($example))
            <hr>
            <a href="{{ Route('frontend.file.download' , $files->file_id) }}" class="btn btn-success">دانلود فایل</a>
            @else
            <hr>
                <a href="{{ Route('frontend.plan.index') }}" class="btn btn-success">خرید فایل</a>
            @endif

    @endif



@endsection
