@if(session('notification'))
    <div class="alert alert-success">
        <p>{{ session('notification') }}</p>
    </div>

@endif
