@extends('layouts.frontend')



@section('content')
    @include('Frontend.partials.notification')
    <h4>فایل ها</h4>
    @if($fileItem && count($fileItem) > 0)
        <ul>
            @foreach($fileItem as $files)
                <li>
                    <a href="{{ Route('frontend.file.details' , $files->file_id) }}">{{ $files->file_tittle }}</a>
                </li>
            @endforeach
        </ul>
    @endif
    <hr>
    <h4>پکیج ها</h4>
    @if($packageItem && count($packageItem) > 0)
        <ul>
            @foreach($packageItem as $packages)
                <li>
                    <a href="#">{{ $packages->package_tittle }}</a>
                </li>
            @endforeach
        </ul>
    @endif


@endsection
