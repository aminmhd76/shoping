@switch(session('create'))
    @case('user')
    <div class="alert alert-success">
        <p>کاربر با موفقیت ایجاد گردید.</p>
    </div>
    @break
    @case('file')
    <div class="alert alert-success">
        <p>فایل موردنظر با موفقیت ایجاد گردید.</p>
    </div>
    @break
    @case('plan')
    <div class="alert alert-success">
        <p>طرح موردنظر با موفقیت ایجاد گردید.</p>
    </div>
    @break
    @case('package')
    <div class="alert alert-success">
        <p>پکیج موردنظر با موفقیت ایجاد گردید.</p>
    </div>
    @break
    @case('category')
    <div class="alert alert-success">
        <p>دسته بندی با موفقیت ایجاد شد.</p>
    </div>
    @break
@endswitch


@switch(session('delete'))
    @case('user')
    <div class="alert alert-success">
        <p>کاربر با موفقیت حذف گردید.</p>
    </div>
    @break
    @case('file')
    <div class="alert alert-success">
        <p>فایل موردنظر با موفقیت حذف گردید.</p>
    </div>
    @break
    @case('plan')
    <div class="alert alert-success">
        <p>طرح موردنظر با موفقیت حذف گردید.</p>
    </div>
    @break
    @case('package')
    <div class="alert alert-success">
        <p>پکیج موردنظر با موفقیت حذف گردید.</p>
    </div>
    @break
    @case('category')
    <div class="alert alert-success">
        <p>دسته بندی مورد نظر حذف شد.</p>
    </div>
    @break
@endswitch


@switch(session('edit'))
    @case('user')
    <div class="alert alert-success">
        <p>کاربر مورد نظر با موفقیت ویرایش شد.</p>
    </div>
    @break
    @case('plan')
    <div class="alert alert-success">
        <p>طرح مورد نظر با موفقیت ویرایش شد.</p>
    </div>
    @break
    @case('package')
    <div class="alert alert-success">
        <p>پکیج مورد نظر با موفقیت ویرایش شد.</p>
    </div>
    @break
    @case('category')
    <div class="alert alert-success">
        <p>پکیج مورد نظر به روزرسانی شد.</p>
    </div>
    @break


@endswitch

@if(session('sync'))
    <div class="alert alert-success">
        <p>فایل های مورد نظر با موفقیت انتخاب شدن.</p>
    </div>
@endif




