<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ Route('admin.user.first') }}">مدیریت فروشگاه</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">کاربران <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ Route('admin.user.create') }}">ثبت کاربر</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ Route('admin.user.list') }}">لیست کاربران</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">ثبت طرح های اشتراکی<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ Route('admin.plan.create') }}">ثبت طرح</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ Route('admin.plan.list') }}">لیست طرح ها</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">پرداخت ها<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ Route('admin.payment.list') }}">لیست پرداخت ها</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">فایل ها<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ Route('admin.file.create') }}">ثبت فایل</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ Route('admin.file.index') }}">لیست فایل ها</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">پکیج ها<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ Route('admin.package.create') }}">ثبت پکیج</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ Route('admin.package.list') }}">لیست پکیج ها</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">دسته بندی ها<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ Route('admin.category.create') }}">ثبت دسته بندی</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ Route('admin.category.list') }}">لیست دسته بندی ها</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
