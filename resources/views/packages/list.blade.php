@extends('layouts.admin')



@section('content')

    @include('partials.notifications')

    @if($packages && count($packages)>0)
        <table style="text-align: center" class="table table-bordered">
            <thead>
            @include('packages.columns')
            </thead>
        @foreach($packages as $package)
            @include('packages.database' ,$package)

        @endforeach

        </table>

    @endif


@endsection
