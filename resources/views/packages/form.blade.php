<div class="row">
    <div class="col-xs-12 col-md-6">
        @include('partials.error')
        <form action="" method="post">
            @csrf
            <div class="form-group">
                <label for="package_tittle">عنوان پکیج :</label>
                <input class="form-control" id="package_tittle" name="package_tittle" type="text"
                       value="{{ old('package_tittle' , isset($packageItem) ? $packageItem->package_tittle : '') }}">
            </div>

            <div class="form-group">
                <label for="package_price">قیمت پکیج :</label>
                <input class="form-control" id="package_price" name="package_price" type="text"
                       value="{{ old('package_price' , isset($packageItem) ? $packageItem->package_price : '') }}">
            </div>
            <div class="form-group">
                <lable for="categories">دسته بندی ها :</lable>
                <select name="categories[]" id="categories" class="form-control select2" size="4" multiple="multiple">
                    @if($categories && count($categories) > 0)
                        @foreach($categories as $category)
                            <option
                                value="{{ $category->category_id }}" {{ isset($category_add) && in_array($category->category_id , $category_add) ? 'selected' : '' }}>
                                {{ $category->category_name }}
                            </option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="form-group">
                <input type="submit" name="submit" class="btn btn-success" value="ذخیره اطلاعات">
            </div>
        </form>
    </div>
</div>

