<a href="{{ Route('admin.package.delete' , $package->package_id) }}"><i class="glyphicon glyphicon-trash"></i></a>
<a href="{{ Route('admin.package.edit' , $package->package_id) }}"><i class="glyphicon glyphicon-edit"></i></a>
<a href="{{ Route('admin.package.sync', $package->package_id) }}"><i class="glyphicon glyphicon-open-file"></i></a>
