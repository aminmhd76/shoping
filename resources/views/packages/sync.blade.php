@extends('layouts.admin')



@section('content')

    @if($packageItem  && count($packageItem) > 0)
        <form action="" method="post">
            @csrf
            @foreach($packageItem as $file)

                <div class="form-group" >
                    <div class="pretty p-switch p-fill">
                        <input type="checkbox" value="{{ $file->file_id }}" name="file[]" {{ isset($isset_file) && in_array($file->file_id , $isset_file) ? 'checked' : '' }}>
                        <div class="state">
                            <label>{{ $file->file_tittle }}</label>
                        </div>
                    </div>
                </div>

            @endforeach

            <div class="form-group">
                <input type="submit" name="submit" class="btn btn-success" value="ذخیره اطلاعات">
            </div>

        </form>


    @endif


@endsection
