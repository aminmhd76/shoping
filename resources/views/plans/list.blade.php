@extends('layouts.admin')



@section('content')

    @include('partials.notifications')

    @if($plans && count($plans)>0)

        <table style="text-align: center" class="table table-bordered">
            <thead>
            @include('plans.columns')
            </thead>
            @foreach($plans as $plan)
                @include('plans.database',$plan)
            @endforeach

        </table>

    @endif



@endsection
