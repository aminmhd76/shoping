<div class="row">
    <div class="col-xs-12 col-md-6">
        @include('partials.error')
        <form action="" method="post">
            @csrf
            <div class="form-group">
                <label for="plan_tittle">عنوان طرح :</label>
                <input class="form-control" id="plan_tittle" name="plan_tittle"
                       type="text" value="{{ old('plan_tittle', isset($planItem)? $planItem->plan_tittle : '') }}">
            </div>
            <div class="form-group">
                <label for="plan_limit_download_count">محدودیت دانلود روزانه :</label>
                <input class="form-control" id="plan_limit_download_count" name="plan_limit_download_count"
                       type="text" value="{{ old('plan_limit_download_count' , isset($planItem) ? $planItem->plan_limit_download_count : '') }}">
            </div>
            <div class="form-group">
                <label for="plan_price">قیمت :</label>
                <input class="form-control" id="plan_price" name="plan_price" type="text"  value="{{ old('plan_price' , isset($planItem) ? $planItem->plan_price : '') }}">
            </div>
            <div class="form-group">
                <label for="plan_days_count">تعداد روز اعتبار :</label>
                <input class="form-control" id="plan_days_count" name="plan_days_count" type="text" value="{{ old('plan_days_count' , isset($planItem) ? $planItem->plan_days_count : '') }}">
            </div>
            <div class="form-group">
                <input type="submit" name="submit" class="btn btn-success" value="ذخیره اطلاعات">
            </div>
        </form>
    </div>
</div>

