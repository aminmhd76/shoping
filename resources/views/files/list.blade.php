@extends('layouts.admin')

@section('content')
    @include('partials.notifications')
    @if($files && count($files) > 0)

        <table style="text-align: center" class="table table-bordered">
            <thead>
            @include('files.columns')
            </thead>
            @foreach($files as $file)
                @include('files.database' , $file)
            @endforeach


        </table>

    @endif




@endsection
