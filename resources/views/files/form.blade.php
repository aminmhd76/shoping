<div class="row">
    <div class="col-xs-12 col-md-6">
        @include('partials.error')
        <form action="" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="name">نام فایل :</label>
                <input class="form-control" id="name" name="file_tittle" type="text">
            </div>
            <div class="form-group">
                <label for="file_description">توضیحات فایل :</label>
                <textarea name="file_description" id="file_description" class="form-control" cols="30"
                          rows="10"></textarea>
            </div>
            <div class="form-group">
                <label for="file">فایل :</label>
                <input type="file" name="fileItem">
            </div>

            <div class="form-group">
                <input type="submit" name="submit" class="btn btn-success" value="ذخیره اطلاعات">
            </div>
        </form>
    </div>
</div>

