@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-xs-12 col-md-6">
            @include('partials.error')
            <form action="" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">نام کامل :</label>
                    <input class="form-control" id="name" name="name" type="text" value="{{ old('name' , isset($editItem) ? $editItem->name : '') }}">
                </div>
                <div class="form-group">
                    <label for="email">ایمیل :</label>
                    <input class="form-control" id="email" name="email" type="email" value="{{ old('email' , isset($editItem) ? $editItem->email : '') }}">
                </div>
                <div class="form-group">
                    <label for="password">کلمه عبور :</label>
                    <input class="form-control" id="password" name="password" type="password">
                </div>
                <div class="form-group">
                    <label for="role">نقش کاربری :</label>
                    <select class="form-control" id="role" name="role">
                        <option value="1" {{ isset($editItem) && $editItem->role == 1 ? 'selected' : '' }}>کاربر عادی</option>
                        <option value="2" {{ isset($editItem) && $editItem->role == 2 ? 'selected' : '' }}>اپراتور</option>
                        <option value="3" {{ isset($editItem) && $editItem->role == 3 ? 'selected' : '' }}>مدیریت</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="wallet">کیف پول :</label>
                    <input class="form-control" id="wallet" name="wallet" type="text" value="{{ old('wallet' , isset($editItem) ? $editItem->wallet : 0) }}">
                </div>
                <div class="form-group">
                    <input type="submit" name="submit" class="btn btn-success" value="ذخیره اطلاعات">
                </div>
            </form>
        </div>
    </div>


@endsection
