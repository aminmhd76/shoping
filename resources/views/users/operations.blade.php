<a href="{{ Route('admin.user.delete', $user->id) }}"><i class="glyphicon glyphicon-trash"></i></a>
<a href="{{ Route('admin.user.edit', $user->id) }}"><i class="glyphicon glyphicon-edit"></i></a>
<a title="لیست پکیج ها" href="{{ Route('admin.user.package', $user->id) }}"><i class="glyphicon glyphicon-list-alt"></i></a>
