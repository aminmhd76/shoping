@extends('layouts.admin')

@section('content')
    @include('partials.notifications')
    @if($users && count($users) > 0)
        <table style="text-align: center" class="table table-bordered">
            <thead>
            <tr>
                <th style="text-align: center">شناسه</th>
                <th style="text-align: center">نام</th>
                <th style="text-align: center">ایمیل</th>
                <th style="text-align: center">موجودی</th>
                <th style="text-align: center">پکیج های خریداری شده</th>
                <th style="text-align: center">عملیات</th>
            </tr>
            </thead>
            @foreach($users as $user)
                @include('users.database',$user)
            @endforeach
        </table>
    @endif
@endsection
