@extends('layouts.admin')


@section('content')
    <table  style="text-align: center" class="table table-bordered">

        <tr>
            <th style="text-align: center">پکیج</th>
            <th style="text-align: center">مبلغ پرداخت شده</th>
            <th style="text-align: center">تاریخ پرداخت</th>
        </tr>

        @if($user_package && count($user_package)>0)
            @foreach($user_package as $package)

                <tr>

                    <td>{{ $package->package_tittle }}</td>
                    <td>{{number_format( $package->pivot->amount) }}</td>
                    <td>{{ $package->pivot->created_at }}</td>

                </tr>

            @endforeach


            @else

            <tr>
                <td colspan="3">
                    <span>پکیجی خریداری نشده است</span>
                </td>
            </tr>



        @endif


    </table>




@endsection





