<tr>
    <td>{{ $user->id }}</td>
    <td>{{ $user->name }}</td>
    <td>{{ $user->email }}</td>
    <td>{{ $user->wallet }}</td>
    <td>{{ $user->packages()->get()->count() }}</td>
    <td style="text-align: center">

        @include('users.operations', $user)
    </td>
</tr>
