@extends('layouts.admin')



@section('content')


    @if($paymentItem  && count($paymentItem)>0)
        <table style="text-align: center" class="table table-bordered">
            <thead>
            @include('payments.columns')
            </thead>

            @foreach($paymentItem as $payments)
                @include('payments.database' , $payments)
            @endforeach

        </table>
    @else
        <div class="alert alert-success">
            <p>فعلا هیچ پرداختی وجود ندارد</p>
        </div>




    @endif




@endsection
