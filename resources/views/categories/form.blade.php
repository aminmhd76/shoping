@extends('layouts.admin')


@section('content')


    <div class="row">
        <div class="col-xs-12 col-md-6">
            @include('partials.error')
            <form action="" method="post">
                @csrf
                <div class="form-group">
                    <label for="package_tittle">عنوان دسته بندی :</label>
                    <input class="form-control" id="package_tittle" name="category_name" type="text" value="{{ old('category_name' , isset($edit_category) ? $edit_category->category_name : '') }}">
                </div>

                <div class="form-group">
                    <input type="submit" name="submit" class="btn btn-success" value="ذخیره اطلاعات">
                </div>
            </form>
        </div>
    </div>




@endsection
