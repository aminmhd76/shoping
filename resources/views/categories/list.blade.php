@extends('layouts.admin')


@section('content')

    @include('partials.notifications')
    @if( $category_list && count($category_list) > 0 )
        <table style="text-align: center" class="table table-bordered">
            <thead>
            @include('categories.columns')
            </thead>
            @foreach($category_list as $category)
                @include('categories.database' , $category)
            @endforeach

        </table>

    @endif






@endsection
