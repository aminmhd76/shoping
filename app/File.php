<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use App\Traits\Category;

class File extends Model
{
    use Category;
    protected $primaryKey = 'file_id';
    protected $guarded = ['file_id'];

    public function packages()
    {
        return $this->belongsToMany(Package::class, 'file_package', 'file_id', 'package_id');
    }

    public function getFileTypeAttribute()
    {
        $Types = [
            'image/jpeg' => 'JPG',
            'application/pdf' => 'PDF'
        ];
        return $Types[ $this->attributes['file_type'] ];

    }


}
