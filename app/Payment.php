<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $primaryKey = 'payment_id';
    const CREATED_AT = 'payment_created_at';
    const  UPDATED_AT = 'payment_updated_at';

    const ONLINE = 1;
    const WALLET = 2;


    public function user()
    {
        return $this->belongsTo(User::class, 'payment_user_id');
    }

    public function payment()
    {
        switch ($this->attributes['payment_method']) {

            case self::ONLINE:
                return 'انلاین';
                break;
            case self::WALLET:
             return 'کیف پول';
                break;


        }


    }

}
