<?php


namespace App\Utility;

use App\Subscribe;
use App\User;
use Carbon\Carbon;


class Utility
{
    public static function is_user_subscribed(int $user_id)
    {
        if (!$user_id) {
            return false;
        }
        $users = User::find($user_id);
        $user_subscribe = $users->subscribes()->where('subscribe_expired_at', '>=', Carbon::now())->first();
        //  dd($user_subscribe);
        return (!empty($user_subscribe));


    }


}
