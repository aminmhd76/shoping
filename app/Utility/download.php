<?php


namespace App\Utility;

use App\User;


class download
{
    public static function user_can_download(int $user_id)
    {
        if (!Utility::is_user_subscribed($user_id)) {
            return false;
        }
        $users = User::find($user_id);
        $user_subscribe = $users->subscribes()->where('subscribe_expired_at', '>=', Carbon::now())->first();
        if (!$user_subscribe) {
            return false;
        }
        if ($user_subscribe->subscribe_download_count == $user_subscribe->subscribe_download_limit) {
            return false;
        }
        return true;


    }


}
