<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class Arcmiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->role == User::ADMIN) {
            return $next($request);
        } else {
            return redirect('/');
        }

        //  if (empty(Auth::user())) {
        //       return redirect()->Route('frontend.home')->with(['notification' => 'شما هنوز لاگین نکرده اید.']);
        //   }


    }
}
