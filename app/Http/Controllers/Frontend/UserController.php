<?php

namespace App\Http\Controllers\Frontend;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function login()
    {
        return view('Frontend.user.login')->with(['dashboard_panel' => 'ورود کابر']);
    }

    public function doLogin(Request $request)
    {
        $a = Auth::attempt(['email' => $request->input('email') , 'password' => $request->input('password')]);
        dd($a);

    /*    if () {
            return redirect('/')->with(['notification' => 'شما با موفقیت لاگین شدید.']);
        }else{
            return redirect('/');
        }*/

    }

    public function register()
    {
        return view('Frontend.user.register')->with(['dashboard_panel' => 'ثبت نام']);
    }

    public function doRegister(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required',
        ]);
        $user_register = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];
        $user_create = User::create($user_register);
        if ($user_create instanceof User) {
            return redirect('/')->with(['notification' => 'شما با موفقیت ثبت نام شدید.']);
        }


    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }


}
