<?php

namespace App\Http\Controllers\Frontend;

use App\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\File;

class HomeController extends Controller
{
    public function index()
    {
        $fileItem = File::all();
        $packageItem = Package::all();
        return view('frontend.home.index' , compact('fileItem' , 'packageItem'))->with(['dashboard_panel' => 'اطلاعات']);

    }


}
