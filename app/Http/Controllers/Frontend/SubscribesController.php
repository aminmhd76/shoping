<?php

namespace App\Http\Controllers\Frontend;

use App\Plan;
use App\Subscribe;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscribesController extends Controller
{
    public function register($plan_id)
    {
        if (ctype_digit($plan_id)) {
            $subscribe = Plan::find($plan_id);
            if ($subscribe instanceof Plan) {
                return view('frontend.subscribes.index', compact('subscribe'))->with(['dashboard_panel' => 'اطلاعات خرید']);
            }


        }

    }

    public function index(Request $request,int $plan_id)
    {
            $plan_attribute = Plan::find($plan_id);
            if (!$plan_attribute ){
                return redirect()->Route('frontend.home')->with(['notification' => 'طرحی برای خرید وجود ندارد.']);
            }

            if ($plan_attribute instanceof Plan){
                $plan_days_add = $plan_attribute->plan_days_count;
                $expired_at = Carbon::now();
                $expired_at->addDays($plan_days_add);
                $subscribes = [
                    'subscribe_user_id' => 2,
                    'subscribe_plan_id' => $plan_id,
                    'subscribe_download_limit' => $plan_attribute->plan_limit_download_count,
                    'subscribe_created_at' => Carbon::now(),
                    'subscribe_expired_at' => $expired_at,
                ];
                $subscribe_add_table = Subscribe::create($subscribes);
                return redirect()->Route('frontend.home')->with(['notification' => 'طرح مورد نظر خریداری شد.']);

            }



    }
}
