<?php

namespace App\Http\Controllers\Frontend;

use App\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class FilesController extends Controller
{
    public function details($file_id)
    {
        if (ctype_digit($file_id)) {
            $files = File::find($file_id);
            $example = 2;
            if ($files instanceof File) {
                return view('Frontend.files.single', compact('files', 'example'))->with(['dashboard_panel' => 'جزعیات فایل ها']);
            }
        }

    }

    public function download(int $file_id)
    {
//        $user = 2;
//        if (!App\Utility\download::user_can_download($user)) {
//           return abort(404);
//        }
        $fileItem = File::find($file_id);
        if (!$fileItem) {
            return redirect()->Route('frontend.home')->with(['notification' => 'فایل مورد نظر معتبر نمی باشد.']);
        }

        return response()->download(storage_path().'/app/images/'.$fileItem->file_name);


    }


}
