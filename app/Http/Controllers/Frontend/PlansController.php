<?php

namespace App\Http\Controllers\Frontend;

use App\Plan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlansController extends Controller
{
    public function index()
    {
        $plan_Item = Plan::all();
        if (!empty($plan_Item)) {
            return view('Frontend.Plans.plan', compact('plan_Item'))->with(['dashboard_panel' => 'طرح های اشتراکی']);
        }
    }


}
