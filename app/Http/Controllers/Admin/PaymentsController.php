<?php

namespace App\Http\Controllers\Admin;

use App\Payment;
use Illuminate\Http\Request;
use App\Http\controllers\controller;

class PaymentsController extends Controller
{
    public function list()
    {
        $paymentItem = Payment::all();
        return view('payments.index', compact('paymentItem'))->with(['dashboard_panel' => 'لیست پرداخت ها']);

    }


}
