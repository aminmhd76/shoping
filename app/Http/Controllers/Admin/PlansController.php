<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Planrequest;
use App\Plan;
use Illuminate\Http\Request;
use App\Http\controllers\controller;

class PlansController extends Controller
{
    public function create()
    {
        return view('plans.create')->with(['dashboard_panel' => 'ثبت طرح']);

    }

    public function store(Planrequest $request)
    {

        $create_plan = [
            'plan_tittle' => $request->input('plan_tittle'),
            'plan_limit_download_count' => $request->input('plan_limit_download_count'),
            'plan_price' => $request->input('plan_price'),
            'plan_days_count' => $request->input('plan_days_count')
        ];
        $add_database = Plan::create($create_plan);

        if ($add_database && $add_database instanceof Plan) {
            return redirect()->Route('admin.plan.list')->with(['create' => 'plan']);

        }
    }

    public function list()
    {
        $plans = Plan::all();
        return view('plans.list', compact('plans'))->with(['dashboard_panel' => 'لیست طرح ها']);

    }

    public function delete($plan_id)
    {
        if ($plan_id && ctype_digit($plan_id)) {
            $destroy = Plan::find($plan_id);
            if ($destroy && $destroy instanceof Plan) {
                $destroy->delete();
                return redirect()->Route('admin.plan.list')->with(['delete' => 'plan']);
            }
        }
    }

    public function edit($plan_id)
    {
        if (ctype_digit($plan_id)) {

            $planItem = Plan::find($plan_id);
            if ($planItem instanceof Plan) {
                return view('plans.create', compact('planItem'))->with(['dashboard_panel' => 'ویرایش طرح']);
            }
        }
    }

    public function update(Request $request, $plan_id)
    {
        $edit_plan = [
            'plan_tittle' => $request->input('plan_tittle'),
            'plan_limit_download_count' => $request->input('plan_limit_download_count'),
            'plan_price' => $request->input('plan_price'),
            'plan_days_count' => $request->input('plan_days_count'),
        ];
        if (ctype_digit($plan_id)) {
            $update_plan = Plan::find($plan_id);
            if ($update_plan instanceof Plan){
                $update_plan->update($edit_plan);
                return redirect()->Route('admin.plan.list')->with(['edit' => 'plan']);
            }
        }


    }


}
