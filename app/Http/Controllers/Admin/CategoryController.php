<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\controllers\controller;

class CategoryController extends Controller
{
    public function create()
    {
        return view('categories.form')->with(['dashboard_panel' => 'ثبت دسته بندی جدید']);
    }

    public function store(Request $request)
    {
        $form_category = ['category_name' => $request->input('category_name')];
        $create_category = Category::create($form_category);
        if ($create_category instanceof Category) {
            return redirect()->Route('admin.category.list')->with(['create' => 'category']);
        }

    }

    public function list()
    {
        $category_list = Category::all();
        if ($category_list) {
            return view('categories.list', compact('category_list'))->with(['dashboard_panel' => 'لیست دسته بندی ها']);
        }
    }

    public function delete($category_id)
    {
        if (ctype_digit($category_id)) {
            $category_delete = Category::find($category_id);
            if ($category_delete instanceof Category) {
                $category_delete->delete();
                return redirect()->Route('admin.category.list')->with(['delete' => 'category']);
            }
        }
    }

    public function edit($category_id)
    {
        if (ctype_digit($category_id)) {
            $edit_category = Category::find($category_id);
            if ($edit_category instanceof Category) {
                return view('categories.form', compact('edit_category'))->with(['dashboard_panel' => 'ویرایش دسنه بندی ها. ']);
            }


        }

    }

    public function update(Request $request, $category_id)
    {
        $update_category = [
            'category_name' => $request->input('category_name'),
        ];
        $category_update_find = Category::find($category_id);
        if ($category_update_find instanceof Category) {
            $category_update_find->update($update_category);
            return redirect()->Route('admin.category.list')->with(['edit' => 'category']);

        }

    }


}
