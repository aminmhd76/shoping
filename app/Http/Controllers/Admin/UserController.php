<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Userrequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\controllers\controller;

class UserController extends Controller
{
    public function index()
    {
        return view('admin.dashboard.index');
    }

    public function create()
    {
        return view('users.user')->with(['dashboard_panel' => 'ثبت کاربر جدید']);
    }

    public function list()
    {
        $users = User::all();
        return view('users.list', compact('users'))->with(['dashboard_panel' => 'لیست کاربران']);
    }

    public function store(Userrequest $userrequest)
    {
        $user_create = [
            'name' => $userrequest->input('name'),
            'email' => $userrequest->input('email'),
            'password' => $userrequest->input('password'),
            'wallet' => $userrequest->input('wallet'),
            'role' => $userrequest->input('role')
        ];
        $user_create = User::create($user_create);
        if ($user_create instanceof User) {
            return redirect()->Route('admin.user.list')->with(['create' => 'user']);
        }
    }

    public function delete($user_id)
    {
        if ($user_id && ctype_digit($user_id)) {
            $userItem = User::find($user_id);
            if ($userItem instanceof User) {
                $userItem->delete();
                return redirect()->Route('admin.user.list')->with(['delete' => 'user']);
            }
        }
    }

    public function edit($user_id)
    {
        if ($user_id && ctype_digit($user_id)) {
            $editItem = User::find($user_id);
            if ($editItem instanceof User) {
                return view('users.edit', compact('editItem'))->with(['dashboard_panel' => 'ویرایش کاربر']);
            }
        }

    }

    public function update(Userrequest $update, $user_id)
    {

        $input = [
            'name' => $update->input('name'),
            'email' => $update->input('email'),
            'password' => $update->input('password'),
            'wallet' => $update->input('wallet'),
            'role' => $update->input('role')
        ];
        // if( !request()->has('password'){}
        if (empty($input['password'])) {
            unset($input['password']);
        }
        $userUpdate = User::find($user_id);
        $userUpdate->update($input);
        return redirect()->Route('admin.user.list')->with(['edit' => 'user']);


    }

    public function package(Request $request , $user_id)
    {
        $package = User::find($user_id);
        if ($package && $package instanceof User){
            $user_package = $package->packages()->get();
            return view('users.package', compact('user_package'))->with(['dashboard_panel' => 'لیست پکیج ها']);
        }

    }
}
