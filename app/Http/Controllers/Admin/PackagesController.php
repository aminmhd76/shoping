<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\File;
use App\Package;
use Illuminate\Http\Request;
use App\Http\controllers\controller;

class PackagesController extends Controller
{
    public function create()
    {
        $categories = Category::all();
        return view('packages.index', compact('categories'))->with(['dashboard_panel' => 'ثبت پکیج']);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'package_tittle' => 'required',
            'package_price' => 'required'
        ], [
            'package_tittle.required' => 'وارد کردن عنوان پکیج الزامیست.',
            'package_price.required' => 'وارد کردن قیمت پکیج الزامیست.'
        ]);


        $create_packages = [
            'package_tittle' => $request->input('package_tittle'),
            'package_price' => $request->input('package_price')
        ];
        $create = Package::create($create_packages);
        if ($create instanceof Package) {
            if ($request->has('categories')) {
                $request_category = $request->get('categories');
                $create->categories()->sync($request_category);
            }
            return redirect()->Route('admin.package.list')->with(['create' => 'package']);
        }
    }


    public function list()
    {
        $packages = Package::all();
        if (count($packages) > 0) {
            return view('packages.list', compact('packages'))->with(['dashboard_panel' => 'لیست پکیج ها']);
        }
    }

    public function delete($package_id)
    {
        if ($package_id && ctype_digit($package_id)) {
            $delete_package = Package::find($package_id);
            if ($delete_package instanceof Package) {
                $delete_package->delete();
                return redirect()->Route('admin.package.list')->with(['delete' => 'package']);
            }

        }
    }

    public function edit($package_id)
    {
        if ($package_id && ctype_digit($package_id)) {
            $packageItem = Package::find($package_id);
            if ($packageItem && $packageItem instanceof Package) {
                $category_add = $packageItem->categories()->get()->pluck('category_id')->toArray();
                $categories = Category::all();
                return view('packages.index', compact('packageItem', 'categories', 'category_add'))->with(['dashboard_panel' => 'ویرایش پکیج']);
            }
        }
    }

    public function update(Request $request, $package_id)
    {
        $this->validate($request, [
            'package_tittle' => 'required',
            'package_price' => 'required'
        ], [
            'package_tittle.required' => 'وارد کردن عنوان پکیج الزامیست.',
            'package_price.required' => 'وارد کردن قیمت پکیج الزامیست.'
        ]);
        $update_package = [
            'package_tittle' => $request->input('package_tittle'),
            'package_price' => $request->input('package_price')
        ];
        if ($package_id && ctype_digit($package_id)) {
            $find_package = Package::find($package_id);
            if ($find_package instanceof Package) {
                $find_package->update($update_package);
                if ($request->has('categories')) {
                    $find_package->categories()->sync($request->get('categories'));
                }
                return redirect()->Route('admin.package.list')->with(['edit' => 'package']);
            }
        }
    }

    public function sync(Request $request, $package_id)
    {
        $packageItem = File::all();
        $package_choose = Package::find($package_id);
        $isset_file = $package_choose->files()->get()->pluck('file_id')->toArray();
        return view('packages.sync', compact('packageItem', 'isset_file'))->with(['dashboard_panel' => 'انتخاب فایل']);
    }

    public function choose(Request $request, $package_id)
    {
        $package_file = Package::find($package_id);
        $package_request = $request->input('file');
        if ($package_file instanceof Package && is_array($package_request) && count($package_request) > 0) {
            $package_file->files()->sync($package_request);
            return redirect()->Route('admin.package.list')->with(['sync' => 'file']);

        }


    }


}
