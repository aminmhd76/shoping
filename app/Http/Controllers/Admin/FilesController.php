<?php

namespace App\Http\Controllers\Admin;

use App\File;
use Illuminate\Http\Request;
use App\Http\controllers\controller;
use Illuminate\Support\Str;

class FilesController extends Controller
{

    public function index()
    {
        $files = File::all();
        return view('files.list', compact('files'))->with(['dashboard_panel' => 'لیست فایل ها']);
    }

    public function create()
    {
        return view('files.create')->with(['dashboard_panel' => 'ثبت فایل']);
    }

    public function store(Request $request)
    {


        $this->validate($request, [
            'file_tittle' => 'required',
            'fileItem' => 'required',

        ], [
            'file_tittle.required' => 'وارد کردن نام الزامیست .',
            'fileItem.required' => 'انتخاب کردن فایل الزامیست .',
        ]);
        $new_name_file = str::random(10) . '.' . $request->file('fileItem')->getClientOriginalExtension();
        $request->file('fileItem')->storeAs('images', $new_name_file);


        $create_file = [
            'file_tittle' => $request->input('file_tittle'),
            'file_description' => $request->input('file_description'),
            'file_type' => $request->file('fileItem')->getMimeType(),
            'file_size' => $request->file('fileItem')->getClientSize(),
            'file_name' => $new_name_file,
        ];

        $file_Add_database = File::create($create_file);

        if ($file_Add_database && $file_Add_database instanceof File) {
            return redirect()->Route('admin.file.index')->with(['create' => 'file']);
        }
    }

    public function delete($file_id)
    {
        if ($file_id && ctype_digit($file_id)) {
            $delete_file = File::find($file_id);
            if ($delete_file instanceof File) {
                $delete_file->delete();
                return redirect()->Route('admin.file.index')->with(['delete' => 'file']);
            }
        }

    }
}
