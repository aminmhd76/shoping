<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class Userrequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6|max:13',
        ];
        if (Request()->Route('user_id') && intval(Request()->Route('user_id')) > 0) {
            unset($rules['password']);
        }
        return $rules;


    }

    public function messages()
    {
        return [
            'name.required' => 'وارد کردن نام الزامیست.',
            'email.required' => 'وارد کردن ایمیل الزامیست.',
            'email.email' => 'ایمیل معتبر نیست.',
            'password.required' => 'وارد کردن پسورد الزامیست.',
            'password.min' => 'پسورد باید 6 کاراکتر و بیشتر باشد.',
            'password.max' => 'پسورد باید از 13 کاراکتر کمتر باشد.'
        ];
    }
}
