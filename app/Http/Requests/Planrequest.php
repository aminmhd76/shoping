<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Planrequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'plan_tittle' => 'required',
            'plan_limit_download_count' => 'required',
            'plan_price' => 'required',
            'plan_days_count' => 'required'
        ];
    }

    public function messages()
    {
        return [ 'plan_tittle.required' => 'وارد کردن عنوان طرح الزامیست .',
            'plan_limit_download_count.required' => 'وارد کردن محدودیت دانلود الزامیست .',
            'plan_price.required' => 'وارد کردن قیمت الزامیست .',
            'plan_days_count.required' => 'وارد کردن تعداد روز اعتبار الزامیست .'];
    }

}
