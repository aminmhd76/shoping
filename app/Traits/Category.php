<?php


namespace App\Traits;


trait Category
{
    public function categories()
    {
        return $this->morphToMany(\App\Category::class , 'categorizable');
    }


}
