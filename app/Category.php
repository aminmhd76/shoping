<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    protected $primaryKey = 'category_id';
    protected $guarded = ['category_id'];
    public $timestamps = false;

    public function files()
    {
        return $this->morphedByMany(File::class, 'categorizable');
    }

    public function packages()
    {
        return $this->morphedByMany(Package::class , 'categorizable');
    }

}
