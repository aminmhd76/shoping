<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments' , function (Blueprint $table){
            $table->increments('payment_id');
            $table->integer('payment_user_id')->index();
            $table->smallInteger('payment_method');
            $table->integer('payment_amount');
            $table->dateTime('payment_created_at')->index();
            $table->dateTime('payment_updated_at')->index();
            $table->smallInteger('payment_status')->index();
            $table->string('payment_gateway_name',250);
            $table->string('payment_res_num', 250);
            $table->string('payment_ref_id',250);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
