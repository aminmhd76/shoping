<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscribesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('subscribes' , function (Blueprint $table){
           $table->increments('subscribe_id');
           $table->integer('subscribe_user_id')->index();
           $table->integer('subscribe_plan_id')->index();
           $table->integer('subscribe_download_limit');
           $table->integer('subscribe_id_payment_amount');
           $table->dateTime('subscribe_created_at')->index();
           $table->dateTime('subscribe_expired_at')->index();



       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscribes');
    }
}
